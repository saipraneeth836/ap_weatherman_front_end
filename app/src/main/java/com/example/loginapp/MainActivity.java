package com.example.loginapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText username, password;
    Button login;
    TextView btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
        btn = findViewById(R.id.btnr);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_name, pass;
                user_name = username.getText().toString();
                pass = password.getText().toString();

                if (user_name.equals("")){
                    Toast.makeText(MainActivity.this, "Username is Blank", Toast.LENGTH_LONG).show();
                }
                else if (pass.equals("")){
                    Toast.makeText( MainActivity.this, "Password is Blank", Toast.LENGTH_LONG).show();
                }
                else{
                    // Authentication
                }
            }

        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Register.class);
                startActivity(i);
                finish();

            }
        });

    }
}