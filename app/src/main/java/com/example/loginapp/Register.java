package com.example.loginapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Register extends AppCompatActivity {
    EditText new_user_name, number, new_email_id, new_password;
    Button Reg;
    TextView lbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        new_user_name = findViewById(R.id.username_sign_up);
        number = findViewById(R.id.phone_number);
        new_email_id = findViewById(R.id.email_id);
        new_password = findViewById(R.id.password_sign_up);
        Reg = findViewById(R.id.register);

        Reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name, num, email, Pass;

                Name = new_user_name.getText().toString();
                num = number.getText().toString();
                email = new_email_id.getText().toString();
                Pass = new_password.getText().toString();
                if (Name.equals("")){
                    Toast.makeText(Register.this, "Username is Blank", Toast.LENGTH_LONG).show();
                }
                else if (num.equals("")){
                    Toast.makeText(Register.this, "Number is Blank", Toast.LENGTH_LONG).show();
                }
                else if (email.equals("")){
                    Toast.makeText(Register.this, "Email is Blank", Toast.LENGTH_LONG).show();
                }
                else if (Pass.equals("")){
                    Toast.makeText(Register.this, "Password is Blank", Toast.LENGTH_LONG).show();
                }

            }
        });

    }
}